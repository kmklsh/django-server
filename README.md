[mbentley/ubuntu-django-uwsgi-nginx](https://github.com/mbentley/docker-django-uwsgi-nginx)
* database mysql
* container 서버 폴더 /opt/django/app

#### variable
 * PROJECT_PATH=/opt/app
 * MODULE=app
 * DOMAIN=example.com

```
$ docker run -p 80 -d -e MODULE=myapp PROJECT_PATH=/opt/app DOMAIN=example.com mrlshjjang/django-app-server
```

# docker hub 주소
https://hub.docker.com/r/mrlshjjang/django-server/