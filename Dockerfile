FROM mrlshjjang/ubuntu-12.04:latest
MAINTAINER mrlshjjang <mrlshjjang@gmail.com>
RUN apt-get update
CMD ["echo", "apt-get update finish"]

# passwod 설정없이 넘어가기..
ENV DEBIAN_FRONTEND noninteractive

ADD . /opt/django/

# ngix config
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /opt/django/django.conf /etc/nginx/sites-enabled/
RUN ln -s /opt/django/supervisord.conf /etc/supervisor/conf.d/
CMD ["echo", "django setting finish"]

VOLUME ["/opt/django/app"]

EXPOSE 80 22

CMD ["/opt/django/run.sh"]
