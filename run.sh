#!/bin/bash
set -e

PROJECT_PATH=${PROJECT_PATH:-/opt/app}
MODULE=${MODULE:-website}
DOMAIN=${DOMAIN:-domain}

sed -i "s#module=website.wsgi:application#module=${MODULE}.wsgi:application#g" /opt/django/uwsgi.ini
sed -i "s#project_path#${PROJECT_PATH}#g" /opt/django/uwsgi.ini
sed -i "s#project_path#${PROJECT_PATH}#g" /opt/django/django.conf
sed -i "s#domain#${DOMAIN}#g" /opt/django/django.conf

echo "usgi print"
cat /opt/django/uwsgi.ini

echo "django print"
cat /opt/django/django.conf

exec /usr/bin/supervisord
